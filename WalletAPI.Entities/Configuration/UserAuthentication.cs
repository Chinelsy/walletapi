﻿using System.Collections.Generic;

namespace WalletAPI.Entities.Configuration
{
    public class UserAuthentication
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public bool Success { get; set; }
        public List<string> Errors { get; set; }
    }
}
