﻿using System.ComponentModel.DataAnnotations;

namespace WalletAPI.Entities.Models
{
    public class UserWallet
    {
        [Key]
        public int WalletId { get; set; }
        public int Balance { get; set; }
    }
}
