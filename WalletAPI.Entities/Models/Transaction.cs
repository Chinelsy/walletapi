﻿using System.ComponentModel.DataAnnotations;

namespace WalletAPI.Entities.Models
{
    public class Transaction
    {
        [Key]
        public string Withdraw { get; set; }
        public string Deposite { get; set; }
    }
}
