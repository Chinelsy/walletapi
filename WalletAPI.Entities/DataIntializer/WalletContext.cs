﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WalletAPI.Entities.Models;

namespace WalletAPI.Entities.DataIntializer
{
    public class WalletContext : IdentityDbContext<IdentityUser>
    {
        public WalletContext(DbContextOptions<WalletContext> options)
            : base(options)
        {
        }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<UserWallet> UserWallets { get; set; }
        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }

    }
}
